# Plantilla Latex Matemáticas

## Getting started

This is a template for math papers, it comes with all the essentials to get you started with your math homework, exams, notes, etc.

## Installation
You just need to download this repository and compile the main.tex file. We recommend using VS Code with the LaTeX Workshop extension.